tdaq_package()

tdaq_add_library(ROSMemoryPoolNP 
  src/MemoryPoolNP.cpp
  src/WrapperMemoryPoolNP.cpp
  LINK_LIBRARIES cmem_rcc rcc_error DFExceptions)
