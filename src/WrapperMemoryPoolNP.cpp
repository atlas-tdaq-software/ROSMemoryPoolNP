/*
  ATLAS ROS Software

  Class: WrapperMemoryPoolNP
  Authors: ATLAS ROS group 	
 */

#include "ROSMemoryPoolNP/WrapperMemoryPoolNP.h"
#include "ROSMemoryPoolNP/MemoryPageNP.h"

using namespace ROS ;

WrapperMemoryPoolNP::WrapperMemoryPoolNP(unsigned int noPages,
		unsigned int pageSize,
		unsigned char **virtualAddress,
		unsigned long *physicalAddress,
		int noPagesReserved)
: MemoryPoolNP(noPages,pageSize,noPagesReserved),
  m_poolHandle(0),
  m_vaPool(virtualAddress[0]),
  m_paPool(physicalAddress[0]),
  m_bufferType(0){
	m_pageVector_.reserve(noPages);
	m_initPageVector_.reserve(noPages);
	for (unsigned int i = 0; i < noPages; i++) {
		//printf("Creating Memory Page with virtual 0x%x and physical 0x%x\n",virtualAddress[i],physicalAddress[i]);
		MemoryPageNP * memPage = new MemoryPageNP(this, (char*)virtualAddress[i], pageSize, i, physicalAddress[i]);
		m_pageVector_.push_back(memPage);
		m_initPageVector_.push_back(memPage);
	}

	initVector(&m_pageVector_) ;
	initVectorInitial(&m_initPageVector_) ;
}

WrapperMemoryPoolNP::~WrapperMemoryPoolNP() {
	// test if in use before deleting?   FIXME
	// delete the memory page descriptors
	for (int i=0;i<m_noPages;i++) {
		delete m_initPageVector_[i];
	}
}

unsigned long WrapperMemoryPoolNP::getPhysicalBaseAddress(void) const {
	return m_paPool;
}

unsigned char* WrapperMemoryPoolNP::getVirtualBaseAddress(void) const {
	return m_vaPool;
}

