// -*- c++ -*-  $Id: WrapperMemoryPageNP.h $
/*
  ATLAS ROS Software

  Class: WRAPPERMemoryPageNP
  Authors: ATLAS ROS group 	
*/

#ifndef WRAPPERMEMORYPAGENP_H
#define WRAPPERMEMORYPAGENP_H

#include "ROSMemoryPoolNP/MemoryPageNP.h"
#include "ROSObjectAllocation/FastObjectPool.h"

namespace ROS {
  class WrapperMemoryPageNP : public MemoryPageNP
  {
  public:
    WrapperMemoryPageNP(char * address, unsigned int size,
		      unsigned long physicalAddress=0,
		      void (*releaseAction)(MemoryPageNP *,void *)=0,
		      void *releaseActionParameter=0);
    void * operator new(size_t) ;
    void operator delete(void *memory) ;
  };
  
  inline WrapperMemoryPageNP::WrapperMemoryPageNP(char * address, unsigned int size,
					      unsigned long physicalAddress,
					      void (*releaseAction)(MemoryPageNP *,void *),
					      void *releaseActionParameter ) 
    : MemoryPageNP(address,size,physicalAddress,releaseAction,releaseActionParameter) {
  }
  
  inline void * WrapperMemoryPageNP::operator new(size_t) {
    return FastObjectPool<WrapperMemoryPageNP>::allocate() ; //This is thread UNSAFE!
  }

  inline void WrapperMemoryPageNP::operator delete(void * memory) {
    FastObjectPool<WrapperMemoryPageNP>::deallocate(memory); //This is thread UNSAFE!
  }

}
#endif //WRAPPERMEMORYPAGENP_H

