// -*- c++ -*-  $Id: WrapperMemoryPoolNP.h $
/*
  ATLAS ROS Software

  Class: WRAPPERMEMORYPOOLNP
  Authors: ATLAS ROS group 	
 */


#ifndef WRAPPERMEMORYPOOLNP_H
#define WRAPPERMEMORYPOOLNP_H

#include "ROSMemoryPoolNP/MemoryPoolNP.h"

namespace ROS{

class MemoryPageNP;

/**
 * @stereotype implementationClass 
 */
class WrapperMemoryPoolNP : public MemoryPoolNP {
public:         
	WrapperMemoryPoolNP(unsigned int noPages,unsigned int pageSize,
			unsigned char ** virtualAddress, unsigned long* physicalAddress,
			int noPagesReserved = 0);

	virtual ~WrapperMemoryPoolNP();

	unsigned long getPhysicalBaseAddress(void) const;
	unsigned char *getVirtualBaseAddress(void) const;

private:
	int m_poolHandle;
	unsigned char *m_vaPool;
	unsigned long m_paPool;
	int m_bufferType;
	std::vector <MemoryPageNP *> m_pageVector_;	// the (dynamic) bookkeeping stack
	std::vector <MemoryPageNP *> m_initPageVector_;	// the initial bookkeeping stack
};

}
#endif //WRAPPERMEMORYPOOLNP_H
