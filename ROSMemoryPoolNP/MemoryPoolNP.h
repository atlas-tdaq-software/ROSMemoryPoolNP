// -*- c++ -*-  $Id: MemoryPoolNP.h 98999 2011-04-13 07:10:11Z joos $
/*
  ATLAS ROS Software

  Class: MEMORYPOOLNP
  Authors: ATLAS ROS group 	
 */

#ifndef MEMORYPOOLNP_H
#define MEMORYPOOLNP_H

#include <vector>
#include "MemoryPoolNPException.h"

#include "DFDebug/DFDebug.h"

namespace ROS 
{
class MemoryPageNP;
class EventInputManager;

/**
 * @stereotype interface
 */
class MemoryPoolNP
{
public:
	enum
	{
		ROSMEMORYPOOL_MALLOC = 1,
		ROSMEMORYPOOL_CMEM_GFP,
		ROSMEMORYPOOL_CMEM_BPA
	};
	friend class MemoryPageNP;
	friend class EventInputManager;

	MemoryPoolNP(int noPages, u_int pageSize, int noPagesReserved = 0);
	MemoryPageNP * getPage();
	MemoryPageNP * getPageNonReserved();
	bool isPageAvailable();
	bool isPageAvailableNonReserved();
	int numberOfFreePages();
	int numberOfFreePagesNonReserved();
	int numberOfPages();
	u_int pageSize();
	void dumpVector();
	void reset(void);
	virtual ~MemoryPoolNP();

protected:
	void initVector(std::vector<MemoryPageNP *> *pageVector);
	void initVectorInitial(std::vector<MemoryPageNP *> *pageVector);
	int m_noPages;
	u_int m_pageSize;
	int m_noPagesReserved;

private:
	u_long getPhysicalAddress(int pageNo);
	MemoryPageNP * getPagePointer(int pageNo);
	MemoryPageNP * getPagePointerInitial(int pageNo);
	void freePage(MemoryPageNP * page);

	std::vector<MemoryPageNP *> * m_pageVectorInitial;	// pointer to the initial bookkeeping stack
	std::vector<MemoryPageNP *> * m_pageVector;		// pointer to the bookkeeping stack
	volatile int m_freeIndex;				// and its index
};

inline MemoryPoolNP::MemoryPoolNP(int noPages, u_int pageSize, int noPagesReserved)
: m_noPages(noPages),
  m_pageSize(pageSize),
  m_noPagesReserved(noPagesReserved),
  m_pageVectorInitial(0),
  m_pageVector(0),
  m_freeIndex(0)
{
}

inline int MemoryPoolNP::numberOfPages()
{
	return m_noPages;
}

inline u_int MemoryPoolNP::pageSize()
{
	return m_pageSize;
}

inline bool MemoryPoolNP::isPageAvailable()
{
	return !(m_freeIndex>=m_noPages);
}

inline bool MemoryPoolNP::isPageAvailableNonReserved()
{
	return !(m_freeIndex>= (m_noPages - m_noPagesReserved));
}

inline int MemoryPoolNP::numberOfFreePages()
{
	return (m_noPages - m_freeIndex);
}

inline int MemoryPoolNP::numberOfFreePagesNonReserved()
{

	int nfree = m_noPages - m_noPagesReserved - m_freeIndex;
	if (nfree >= 0)
		return nfree;
	else
		return 0;
}

inline void MemoryPoolNP::initVector(std::vector<MemoryPageNP *> *pageVector)
{
	m_pageVector = pageVector;
}

inline void MemoryPoolNP::initVectorInitial(std::vector<MemoryPageNP *> *pageVector)
{
	m_pageVectorInitial = pageVector;
}

inline MemoryPageNP * MemoryPoolNP::getPage()
{
	if (m_freeIndex >= m_noPages)
		throw MemoryPoolNPException(MemoryPoolNPException::NOPAGESAVAILABLE);

	MemoryPageNP *rc = (*m_pageVector)[m_freeIndex];
	(*m_pageVector)[m_freeIndex] = 0;
	m_freeIndex++;
	return rc;
}

inline MemoryPageNP * MemoryPoolNP::getPageNonReserved()
{
	if (m_freeIndex >= (m_noPages - m_noPagesReserved))
		throw MemoryPoolNPException(MemoryPoolNPException::NOPAGESAVAILABLE);

	MemoryPageNP *rc = (*m_pageVector)[m_freeIndex];
	(*m_pageVector)[m_freeIndex] = 0;
	m_freeIndex++;
	return rc;
}

inline void MemoryPoolNP::freePage(MemoryPageNP * page)
{
	if (m_freeIndex <= 0)
		throw MemoryPoolNPException(MemoryPoolNPException::NOPAGESALLOCATED);

	m_freeIndex--;
	(*m_pageVector)[m_freeIndex] = page;
}
}


#endif //MEMORYPOOLNP_H
