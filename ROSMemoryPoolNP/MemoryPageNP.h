// -*- c++ -*-  $Id: MemoryPageNP.h 99038 2011-04-14 12:17:43Z joos $
/*
  ATLAS ROS Software

  Class: MEMORYPAGE
  Authors: ATLAS ROS group 	
*/

#ifndef MEMORYPAGENP_H
#define MEMORYPAGENP_H

#include "MemoryPoolNP.h"

namespace ROS 
{

struct PageAddress{
	unsigned char *virt;
	unsigned long phys;
};

class MemoryPageNP
  {
  public:
    friend class MemoryPoolNP_malloc;
    friend class MemoryPoolNP_CMEM;
    friend class WrapperMemoryPoolNP;

    void * address() const;
    u_long physicalAddress() const;
    int pageNumber() const;
    u_int usedSize() const;
    u_int capacity() const;
    void lock();
    void free();
    PageAddress reserve(unsigned int size,unsigned int minSize=0);
    //void * reserve(u_int size, u_int minSize = 0);
    int release(u_int size);
    void truncate();
    int referenceCount() const;
    void reset(void);
    void setUserdata(u_int data);
    u_int getUserdata(void);

  protected:  
    MemoryPageNP(char * address, u_int size, u_long physicalAddress = 0, void(*releaseAction)(MemoryPageNP *, void *) = 0,
	       void *releaseActionParameter = 0);
    virtual ~MemoryPageNP() { };
  private:  
    MemoryPageNP(MemoryPoolNP *MemoryPoolNP, char *address, u_int size, int pageNo, u_long physicalAddress = 0);
    MemoryPoolNP * const m_MemoryPoolNP;
    void * const m_address;
    const u_long m_physicalAddress;
    const u_int m_size;
    const int m_pageNo;
    u_int m_used;
    u_int m_usable;
    u_int m_lastReserved;
    u_int m_userData;
    int m_nReferences;
    void(*m_releaseAction)(MemoryPageNP *, void *);
    void *m_releaseActionParameter;
  };
  
  inline MemoryPageNP::MemoryPageNP(char *address, u_int size, u_long physicalAddress,
				void(*releaseAction)(MemoryPageNP *, void *), void *releaseActionParameter)
    :  m_MemoryPoolNP(0),
       m_address(address),
       m_physicalAddress(physicalAddress),
       m_size(size),
       m_pageNo(0),
       m_used(size),
       m_usable(size),
       m_lastReserved(0),
       m_userData(0),
       m_nReferences(1),
       m_releaseAction(releaseAction),
       m_releaseActionParameter(releaseActionParameter)
  {
  }

  inline MemoryPageNP::MemoryPageNP(MemoryPoolNP *MemoryPoolNP, char *address, u_int size, int pageNo, u_long physicalAddress)
    :  m_MemoryPoolNP(MemoryPoolNP),
       m_address(address),
       m_physicalAddress(physicalAddress),
       m_size(size),
       m_pageNo(pageNo),
       m_used(0),
       m_usable(size),
       m_lastReserved(0),
       m_userData(0),
       m_nReferences(1),
       m_releaseAction(0),
       m_releaseActionParameter(0)
  {
  }

  inline void MemoryPageNP::reset(void)
  {
    m_used = 0;
    m_usable = m_size;
    m_lastReserved = 0;
    m_nReferences = 1;
  }

  inline void * MemoryPageNP::address() const
  {
    return m_address;
  }
  
  inline u_long MemoryPageNP::physicalAddress() const
  {
    return m_physicalAddress;
  }
  
  inline u_int MemoryPageNP::usedSize() const
  {
    return m_used;
  }
  
  inline u_int MemoryPageNP::capacity() const
  {
    return(m_usable-m_used);
  }
  
  inline void MemoryPageNP::lock()
  {
    truncate();
    m_nReferences++;
  }
  
  inline void MemoryPageNP::setUserdata(u_int data)
  {
    m_userData = data;
  }
  
  inline u_int MemoryPageNP::getUserdata(void)
  {
   return m_userData; 
  }

  inline void MemoryPageNP::free()
  {
    if((--m_nReferences)==0) 
    {
      if(m_MemoryPoolNP!=0)
      { 
	m_used = 0;
	m_usable = m_size;
	m_lastReserved = 0;
	m_nReferences = 1; 
	m_MemoryPoolNP->freePage(this);
      }
      else {
	if(m_releaseAction!=0) 
	  (*m_releaseAction)(this,m_releaseActionParameter);

	//As there is no MemoryPoolNP to do the cleanup....
        delete this;
      }
    }
  }

  inline PageAddress MemoryPageNP::reserve(unsigned int size,unsigned int minSize) {
  	PageAddress result;
  	if (size <= capacity() ) {
  //		result.virt = (u_long)(((u_long*)m_address)+m_used);
  		result.virt = (unsigned char *)(((unsigned int*)m_address)+m_used);
  		result.phys = m_physicalAddress+(m_used*sizeof(unsigned int));
  		m_used += size ;
  		m_lastReserved = size - minSize;
  	}
  	return result;
  }
  /*
  inline void * MemoryPageNP::reserve(u_int size, u_int minSize)
  {
    void *result = 0;
    if(size <= capacity()) 
    {
      result =(void *)(((char *)m_address) + m_used);
      m_used += size;   
      m_lastReserved = size - minSize;
    }
   return result;
  }
  */
  
  inline int MemoryPageNP::release(u_int size)
  {
    u_int result = 0;
    if( size <= m_lastReserved) 
    { 
      m_used -= size;   
      m_lastReserved -= size;
      result = size;
    }
    return result;
  }
  
  inline void MemoryPageNP::truncate()
  {
    m_lastReserved = 0;
    m_usable = m_used;
  }

  inline int MemoryPageNP::referenceCount() const
  {
    return m_nReferences;
  }
  
  inline int MemoryPageNP::pageNumber() const
  {
    return m_pageNo;
  }
}
#endif //MEMORYPAGENP_H

