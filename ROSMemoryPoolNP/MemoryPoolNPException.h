// -*- c++ -*-
/*
  ATLAS TEST Software

  Class: MEMORYPOOLEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef MEMORYPOOLEXCEPTIONNP_H
#define MEMORYPOOLEXCEPTIONNP_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

class MemoryPoolNPException : public ROSException
{
  
public:
  enum ErrorCode 
  { 
    NOPAGESAVAILABLE,
    NOPAGESALLOCATED,
    ILLEGALPOOLTYPE,
    EXT_ERROR
  };   

  MemoryPoolNPException(ErrorCode error);
  MemoryPoolNPException(ErrorCode error, std::string description);
  MemoryPoolNPException(ErrorCode error, const ers::Context& context);
  MemoryPoolNPException(ErrorCode error, std::string description, const ers::Context& context);
  MemoryPoolNPException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

protected:
  virtual std::string getErrorString(u_int errorId) const;
  virtual ers::Issue * clone() const { return new MemoryPoolNPException( *this ); }
  static const char * get_uid() {return "ROS::MemoryPoolNPException";}
  virtual const char* get_class_name() const {return get_uid();}
};

inline MemoryPoolNPException::MemoryPoolNPException(MemoryPoolNPException::ErrorCode error)
  : ROSException("MemoryPoolNP",error, getErrorString(error)) { }

inline MemoryPoolNPException::MemoryPoolNPException(MemoryPoolNPException::ErrorCode error, std::string description)
  : ROSException("MemoryPoolNP",error, getErrorString(error),description) { }

inline MemoryPoolNPException::MemoryPoolNPException(MemoryPoolNPException::ErrorCode error, const ers::Context& context)
  : ROSException("MemoryPoolNP",error, getErrorString(error), context) { }

inline MemoryPoolNPException::MemoryPoolNPException(MemoryPoolNPException::ErrorCode error, std::string description, const ers::Context& context)
  : ROSException("MemoryPoolNP",error, getErrorString(error),description, context) { }

inline MemoryPoolNPException::MemoryPoolNPException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context)
     : ROSException(cause, "MemoryPoolNP", error, getErrorString(error), description, context) {}

inline std::string MemoryPoolNPException::getErrorString(u_int errorId) const
{
  std::string rc;    
  switch (errorId) 
  {  
    case NOPAGESAVAILABLE:
      rc = "No pages available";
      break;
    case NOPAGESALLOCATED:
      rc = "No pages allocated";
      break;
    case ILLEGALPOOLTYPE:
      rc = "Illegal Pool Type";
      break;
    default:
      rc = "";
      break;
  }
  return rc;
}

#endif //MEMORYPOOLEXCEPTIONNP_H
 
